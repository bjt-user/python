```
>>> import datetime
>>> datetime.datetime.today().strftime('%Y-%m-%d')
'2023-04-29'
>>> datetime.datetime.today()
datetime.datetime(2023, 4, 29, 18, 3, 46, 810551)
>>> datetime.datetime.today().strftime('%Y')
'2023'
```
