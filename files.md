#### read a file

```
file1 = open("newfile.txt", "r")
file_content = file1.read()
file1.close()

print(file_content)
```

#### write to a file

Write to a file and create the file if it does not exist:
```
file1 = open("newfile.txt", "w")

file1.write("hello")
```
(if the file exists, it will overwrite everything)

#### append to a file

```
file1 = open("newfile.txt", "a")

file1.write("\nnext line")
```

#### insert at the head of the file
```
new_content = "the new first line\n"

f = open('newfile.txt','r+')

old_content = f.read()
f.seek(0) # go back to the beginning of the file
f.write(new_content) # write new content at the beginning

f.write(old_content)
f.close()
```

You can use this function:
```
def prepend_to_file(file_name, prepended_text):
    f = open(file_name, "r+")
    old_content = f.read()
    f.seek(0)
    f.write(prepended_text)
    f.write(old_content)
    f.close()

new_content = "1. header\nSome new content:\n"
my_file = "example.txt"

prepend_to_file(my_file, new_content)
```

#### create a file (similar to touch)

https://docs.python.org/3.8/library/functions.html#open

```
open(LOGFILE, 'x')
```

> open for exclusive creation, failing if the file already exists

(On Linux) you can also use:
```
os.mknod('my_file.txt')
```

#### close a file

```
file_handle.close()
```

#### check if file exists

```
>>> os.path.isfile('/etc/os-release')
True
```

But it does not work with globs:
```
>>> os.path.isfile('/etc/os-releas*')
False
```

#### check if file is empty

```
>>> os.stat("emptyfile.txt").st_size == 0
True
```

#### get directory a file is in

```
>>> myfile = '/etc/ssh/sshd_config'
>>> os.path.dirname(myfile)
'/etc/ssh'
```

#### put all files from a dir into a list

```
myfiles = os.listdir(".")
```

#### split file extension

```
>>> os.path.splitext("hello.txt")
('hello', '.txt')
```

remove file extension:
```
>>> os.path.splitext("hello.txt")[0]
'hello'
```

Only get file extension:
```
>>> os.path.splitext("hello.txt")[1]
'.txt'
```
