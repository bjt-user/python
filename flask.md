Flask is a python framework for web development.

#### resources

https://flask.palletsprojects.com/en/2.2.x/

#### table with variable but without borders

The folder structure looks like this:
```
$ tree
.
├── flasktest.py
└── templates
    └── index.html
```
this is the python script that declares and initializes a variable:
```
$ cat flasktest.py 
#!/usr/bin/python3

from flask import Flask, render_template

app = Flask(__name__)

myvar="example text"

@app.route("/")
def index():
  return render_template("index.html", myvar=myvar)

app.run(host="0.0.0.0", port=3000)
```

This is the index.html and you can see where the variable `{{myvar}}` is placed.
```
$ cat templates/index.html 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HTML 5 Boilerplate</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
   <table>
    <tr>
      <th>Company</th>
      <th>Contact</th>
      <th>Country</th>
    </tr>
    <tr>
      <td>Alfreds Futterkiste</td>
      <td>{{myvar}}</td>
      <td>Germany</td>
    </tr>
    <tr>
      <td>Centro comercial Moctezuma</td>
      <td>Francisco Chang</td>
      <td>Mexico</td>
    </tr>
  </table>
  </body>
</html>
```

You can then run the `flasktest.py` and see the table at port 3000.\
Unfortunatelly the table does not have any borders, so it doesnt look that good.\
Also I would need a table with a variable size, so you probably need css for that...

#### include a html file in another html file

Use the
```
{% include "myfile.html" %}
```
syntax.

This worked great for embedding an html file created from `pandas` in an `index.html` file, that has css styles:
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
      table {
        font-family: Ubuntu;
        font-weight: bold;
        background-color: blue;
      }
    </style>
</head>
<body>
  {% include "sql-data.html" %}
</body>
</html>
```

This is how it was called from flask:
```
from flask import Flask, render_template
import create_html

app = Flask(__name__)

@app.route('/')
def sql_table():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3000)
``` 
(and the `create_html.py` created the `sql-data.html` file)

***
## fails
#### tutorials that really suck

-networkchuck

#### tutorials to try

https://blog.miguelgrinberg.com/post/beautiful-interactive-tables-for-your-flask-templates

=> RuntimeError: Working outside of application context.

Like in every other tutorial there is no db, you have to create it first...

#### tutorial free code camp fail

https://www.youtube.com/watch?v=Z1RJmh_OqeA

On Debian it looks like you have to install the packages with `apt`:\
python3-flask\
python3-flask-sqlalchemy

```
sudo apt install python3-flask python3-flask-sqlalchemy
```

```
$ cat app.py 
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "hi world"

if __name__ == "__main__":
    app.run(debug=True)
```
then do
```
python3 app.py
```

And I have "hi world" at http://127.0.0.1:5000/

At minute 19 he creates a database within the virtual environment which does not work if you dont have the venv setup...

The tutorial is very hard if you dont have `sqlalchemy` and `css` experience.

At minute 22 he gets a table in his browser but no idea how he managed to do that...lets try another tutorial.

***
