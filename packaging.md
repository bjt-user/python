## TODO: how to package python projects

https://packaging.python.org/en/latest/tutorials/packaging-projects/

## alternative ideas

Write script or Makefile that puts all python files into one\
and installs that big script to `/usr/local/bin`.\
Only works if everything is in methods.

Create a `wheel`? How does that work?

#### pyinstaller

```
pip3 install --user pyinstaller
```

FAIL: You run `pyinstaller` on your main file that imports your other python files as libs.
```
pyinstaller main.py
```

It will create an elf executable in the current dir in
```
./dist/main/main
```
And that executable worked.\
**But** when copying it to `/usr/local/bin` it did not work anymore\
because this is not a static binary, it relies on the `_internal`\
dir that also is in this `dist` dir.

**This one works:**
```
pyinstaller --onefile main.py
```
This will create just the binary in the `dist` dir and you can execute that from anywhere.

It is a pretty big binary 7M for a very small program,\
but with a speed of around 100ms it was fast enough for a python script.

So you can put this in a Makefile:
```
EXECUTABLE = "my_program"

.PHONY: install
install:
    pyinstaller --onefile --clean main.py --dist ~/.local/bin -n $(EXECUTABLE)
```
and as long as `~/.local/bin` is in your path you can execute it right away.

`--clean` will remove old `build` and `dist` dirs and do a fresh install.
