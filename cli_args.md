#### number of cli args

```
#!/usr/bin/python3

import sys

print (len(sys.argv))
```

#### use cli args

```
#!/usr/bin/python3

import sys

if len(sys.argv) > 1:
    arg1 = sys.argv[1]

    print ("arg1 is: " + arg1)
```

#### output usage when no cli args are given

```
if len(sys.argv) <= 1:
    parser.print_help()
    exit(1)
```

## argparse

The argparse lib is convenient.

Advantage of argparse is that you have an automatic usage.

#### example with an option that has an additional argument

```
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-q', '--quizfile', help='Path to the quiz file')
args = parser.parse_args()

quiz_file = args.quizfile
```


#### how to use argparse for options that do not use an additional argument

You need `action="store_true"` for a simple boolean switch without additional arguments.
```
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-f', '--foo',
    help="this will foo around", action="store_true")

args = parser.parse_args()

print("args:", args)
print("args.foo", args.foo)
```

#### mutual exclusion

https://docs.python.org/3/library/argparse.html#mutual-exclusion

```
Doesnt seem to work for boolean switches:
parser = argparse.ArgumentParser()
command_group = parser.add_mutually_exclusive_group()
command_group.add_argument('run', help='run it', action="store_true")
command_group.add_argument('stop', help='stop it', action="store_true")

args = parser.parse_args()

print(args)
```

```
ValueError: mutually exclusive arguments must be optional
```

#### priority of multiple flags

This will only print "FOO" when both `--foo` and `--bar` are given:
```
parser = argparse.ArgumentParser()

parser.add_argument('-f', '--foo',
    help="this will foo around", action="store_true")

parser.add_argument('-b', '--bar',
    help="this will bar around", action="store_true")

args = parser.parse_args()

print("args:", args)

if args.foo:
    print("FOO")
elif args.bar:
    print("BAR")
```

This would be a way to prioritize the cli args with the if statement.

Maybe there is a better way.

## getopt

Another module which claims to be similar to the getopt function in C:\
https://docs.python.org/3/library/getopt.html#module-getopt
