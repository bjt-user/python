#### general

If you have a twodimensional list (for example a table) each "row" is called a "tupel".

#### define an empty list
```
headers=[]
```

iterate through a list called `myresult`
```
for i in myresult:
    print(i)
```

select element from a twodimensional list:
```
print(myresult[0][1])
```

#### append to a list
```
mylist.append("foo bar")
```

#### define a list of strings and print its length

```
mylist=["apple", "banana", "pizza"]

print(len(mylist))
```

#### print index of an element

```
mylist=["apple", "banana", "pizza"]

print(mylist.index("pizza"))
```

#### insert - insert object before index
First argument of insert is the index\
Second argument of insert is the object you want to put before the index
```
firstlist=["apple", "banana", "pizza"]

firstlist.insert(0,"meat")
print(firstlist)
```
```
['meat', 'apple', 'banana', 'pizza']
```
#### pop - remove and return element at index

```
firstlist=["apple", "banana", "pizza"]

firstlist.pop(1)
print(firstlist)
```

```
['apple', 'pizza']
```

#### sort a list

You can use `.sort()` to permanently sort a list.\
Or use `sorted(mylist)` to print the list sorted.

```
>>> randomlist = ["kiwi", "banana"]
>>> randomlist.sort()
>>> randomlist
['banana', 'kiwi']
```

Or:
```
>>> mylist = ["tree", "rock", "stone", "water", "paper"]
>>> sorted(mylist)
['paper', 'rock', 'stone', 'tree', 'water']
```
