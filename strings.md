#### How to combine a string with a list in a print statement?

```
print ('this is my list: ', mylist)
```

#### concatenate string variables

```
>>> my_var = 'ok' + 'ok'
>>> print(my_var)
okok
```

#### separate a string by delimiter and choose the part

this is similar to a `cut -d'.' -f3`:
```
>>> myvar = 'hello this is a.string.ok'

>>> myvar.split(".")[2]
'ok'
```

`split` creates a list and by directly applying the index you choose the part of the string

#### multiline string

```
>>> another_string = '''<!doctype html>
...                     <html>
...                     <p>hello im under the water</p>
... '''
>>> print(another_string)
<!doctype html>
			<html>
			<p>hello im under the water</p>

```

But how can you indent the string without having a tab inside your variable?

You can use brackets like this:
```
>>> my_string = ("<!doctype html>\n"
...             "<html>\n"
...             "<p>hello im under the water</p>")
>>> print(my_string)
<!doctype html>
<html>
<p>hello im under the water</p>
```

#### contains

How to check if string contains a substring?

Using the `in` operator seems to be best practice.\
https://stackoverflow.com/a/27138045/21120612

```
>>> mystr = 'hello'
>>> 'hell' in mystr
True
>>> 'help' in mystr
False
```

```
if 'hel' in mystr:
    print('bingo')
```

#### make string lower case

```
>>> mystring = "HELLow"
>>> mystring.lower()
'hellow'
```

#### endswith

```
>>> mystring = "foobar"
>>> mystring.endswith("bar")
True
>>> mystring.endswith("foo")
False
```
