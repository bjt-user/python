https://docs.python.org/3/library/unittest.html

#### simple example

Suppose you have a python file named `configuration.py` in which you want to test the\
function `write_config()`.

Create a file named `test_configuration.py` with the following content\
in the same dir of the source file:
```
import unittest
from configuration import write_config

class test_configuration(unittest.TestCase):
    def test_write_config(self):
        my_data = {
            "foo": "bar",
            "test": "ok"
        }
        write_config(my_data, "/tmp/example.conf")
```

Then run:
```
python -m unittest test_configuration.py
```
One dot per test function is printed to the screen.

or for more verbosity:
```
python3 -m unittest -v test_helper.py
```

#### unclosed files

What is really helpful is, that the unittests automatically detect if a file was not closed in a function.

#### TODO: unittests in a separate folder
