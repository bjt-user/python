#### virtual environments

create a new virtual environment
```
python3 -m venv my_virtual_environment
```

This will create a dir named `my_virtual_environment` in the current working directory.\
That directory is 25M in size, contains 183 directories and 1495 files.

It might make sense to create a virtual environment in your home directory to not install \
python modules and to not conflict with system packages?
But I guess you can put the dir anywhere and just source the activate binary.

activate the virtual env:
```
$ source /tmp/my_virtual_environment/bin/activate
(my_virtual_environment) user@host:~ $
```

get out of the virtual env:
```
(my_virtual_environment) user@host:~ $ deactivate
user@host:~ $
```
