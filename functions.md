example for a function with an argument:
```
def read_file(file_name):
    file1 = open(file_name, "r")
    file_content = file1.read()
    file1.close()
    print(file_content)

read_file("newfile.txt")
```
