#### imports and from statements

```
>>> import shutil
>>> copy("/etc/os-release", "/tmp")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'copy' is not defined. Did you forget to import 'copy'?
```

You usually have to give the fully qualified path to the module:
```
>>> import shutil
>>> shutil.copy("/etc/os-release", "/tmp")
'/tmp/os-release'
```

To use the `copy` method without using the fully qualified path you can use the `from ... import ...` syntax:
```
>>> from shutil import copy
>>> copy("/etc/os-release", "/tmp")
'/tmp/os-release'
```

#### example for not using the full path for functions

You can write this code:
```
import sqlalchemy
import sqlalchemy.orm
import sqlalchemy.sql

engine=sqlalchemy.create_engine(f'sqlite:///test.db')

Session = sqlalchemy.orm.sessionmaker(bind=engine)
session = Session()

result_set = session.execute(sqlalchemy.sql.text('SELECT * FROM people'))
rows = result_set.fetchall()

print(rows)
```

also like this:
```
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import text

engine = create_engine(f'sqlite:///test.db')

Session = sessionmaker(bind=engine)
session = Session()

result_set = session.execute(text('SELECT * FROM people'))
rows = result_set.fetchall()

print(rows)
```
so you dont have to use the full path to the functions

***

#### importing another python file

If you have another file that is called `create_file.py` you import it like this in your program:
```
import create_file
```
and its code will be executed before the rest of the current file will be executed.

**PITFALL: The imported file may not have an exit(0) statement at the end.**
