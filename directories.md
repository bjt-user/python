#### create directory with parents

the `mkdir -p` equivalent:
```
>>> import pathlib
>>> pathlib.Path("/tmp/path/to/desired/directory").mkdir(parents=True, exist_ok=True)
```
`exist_ok` means do not throw an error if the dir already exists.

#### rename dirs

```
>>> import os
>>> os.rename("foo", "fooBAR")
```
