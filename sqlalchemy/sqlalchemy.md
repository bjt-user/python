### installation
#### debian
On Debian this apt package was installed:
```
python3-sqlalchemy/unstable,now 1.4.47+ds1-1 all [installed,automatic]
```

```
$ pip3 list | grep -i "alchemy"
Flask-SQLAlchemy   3.0.3
SQLAlchemy         1.4.47
```

apparently sqlalchemy 2.0 is the latest version, but for some reason I only get 1.4 on debian sid...

You can check the current version like this:
```
$ python3
Python 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import sqlalchemy
>>> sqlalchemy.__version__
'1.4.47'
```

https://pypi.org/project/SQLAlchemy/2.0.10/

```
$ pip install SQLAlchemy==2.0.10 --break-system-packages
```

probably use this link https://pypi.org/project/SQLAlchemy/ to know what is the latest version

#### tutorials

NeuralNine: https://www.youtube.com/watch?v=AKQ3XEDI9Mw DONE

**work with vs code to get autocomplete stuff**

This code will generate a sqlite db with a table with two people in it:
```
from sqlalchemy import create_engine, ForeignKey, Column, String, Integer, CHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Person(Base):
    __tablename__ = "people"

    ssn = Column("ssn", Integer, primary_key=True)
    firstname = Column("firstname", String)
    lastname = Column("lastname", String)
    gender = Column("gender", CHAR)
    age = Column("age", Integer)

    def __init__(self, ssn, first, last, gender, age):
        self.ssn = ssn
        self.firstname = first
        self.lastname = last
        self.gender = gender
        self.age = age

    def __repr__(self):
        return f"({self.ssn}) {self.firstname} {self.lastname} ({self.gender},{self.age})"
    
engine = create_engine("sqlite:///test.db", echo=True)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

person1 = Person(12312, "Mike", "Smith", "m", 35)
person2 = Person(12313, "Pete", "Blob", "m", 39)
session.add(person1)
session.add(person2)
session.commit()
```
But it also generates deprecation warnings.

set this environment variable to get more useful warnings:
```
$ export SQLALCHEMY_WARN_20=1
$ python3 main.py 
/home/bf/coding/python/sqlalchemy/main.py:5: MovedIn20Warning: The ``declarative_base()`` function is now available as sqlalchemy.orm.declarative_base(). (deprecated since: 1.4) (Background on SQLAlchemy 2.0 at: https://sqlalche.me/e/b8d9)
```

This is the code without warnings for `2.0.10`:
```
from sqlalchemy import create_engine, ForeignKey, Column, String, Integer, CHAR
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Person(Base):
    __tablename__ = "people"

    ssn = Column("ssn", Integer, primary_key=True)
    firstname = Column("firstname", String)
    lastname = Column("lastname", String)
    gender = Column("gender", CHAR)
    age = Column("age", Integer)

    def __init__(self, ssn, first, last, gender, age):
        self.ssn = ssn
        self.firstname = first
        self.lastname = last
        self.gender = gender
        self.age = age

    def __repr__(self):
        return f"({self.ssn}) {self.firstname} {self.lastname} ({self.gender},{self.age})"
    
engine = create_engine("sqlite:///test.db", echo=True)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

person1 = Person(12312, "Mike", "Smith", "m", 35)
person2 = Person(12313, "Pete", "Blob", "m", 39)
session.add(person1)
session.add(person2)
session.commit()
```

***

#### TODO: read from an existing database

from ChatGPT:
> Regarding your second question, SQLAlchemy does provide an object-relational mapping (ORM) system\
that allows you to map tables to Python classes, and columns to attributes of those classes.\
However, you need to define those classes yourself, and SQLAlchemy will not automatically create them for you.

this code worked for reading a table from an existing db:
```
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import text

engine=sqlalchemy.create_engine(f'sqlite:///test.db')

Session = sessionmaker(bind=engine)
session = Session()

result_set = session.execute(text('SELECT * FROM people'))
rows = result_set.fetchall()

print(rows)
```
***
