Doing things in sqlite3 cli interface directly.

#### describe table
```
sqlite> .schema people
```
```
CREATE TABLE people (
	ssn INTEGER NOT NULL, 
	firstname VARCHAR, 
	lastname VARCHAR, 
	gender CHAR, 
	age INTEGER, 
	PRIMARY KEY (ssn)
);
```
***

#### create table
```
sqlite> create table users (id int primary key, name text, age int, address text, phone text, email text);
```

```
sqlite> .tables
users
sqlite> select * from users;
sqlite> .schema users
CREATE TABLE users (id int primary key, name text, age int, address text, phone text, email text);
```

#### insert into

```
sqlite> insert into users (id, name, age, address, phone, email)
   ...> values (1, "Mike", 12, "Hamington Road", 0190123456, "mike@aol.com");
sqlite> select * from users;
1|Mike|12|Hamington Road|190123456|mike@aol.com
```
