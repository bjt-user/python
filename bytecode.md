To produce bytecode you can open an interactive python shell in the folder of your script and run:
```
>>> import py_compile
>>> py_compile.compile("hw.py")
'__pycache__/hw.cpython-311.pyc'
```

That will create the `__pycache__` folder with bytecode in it.\
That has to be made executable with
```
chmod +x hw.cpython-311.pyc
```
and then you can execute it.

=> the second time I tried this, it failed. (syntax error when trying to execute in WSL container)\
but I was able to run the code with python3:
```
python3 hw.cpython-311.pyc
```
