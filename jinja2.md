#### documentation

https://readthedocs.org/projects/jinja2/downloads/pdf/latest/

TODO: work through example on page 7

#### simple example 1

```
>>> import jinja2
>>> myjinjastring = "Hello {{ name }}!"
>>> template = jinja2.Template(myjinjastring)
>>> my_name = "Carl"
>>> template.render(name=my_name)
'Hello Carl!'
```

Or you can do it shorter like this:
```
>>> import jinja2
>>> myjinjastring = "Hello {{ name }}!"
>>> my_name = "Carl"
>>> template = jinja2.Template(myjinjastring).render(name=my_name)
>>> print(template)
Hello Carl!
```

#### simple example 2

```
>>> import jinja2
>>> env = jinja2.Environment()
>>> company_name = "Wurstbrot AG"
>>> env.from_string("{{ foo }}").render(foo=company_name)
'Wurstbrot AG'
```
