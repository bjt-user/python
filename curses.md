#### example

Display a string at row 5 and col 10 at the screen:
```
#!/usr/bin/python3

import curses
import time

stdscr = curses.initscr()

stdscr.clear()
stdscr.addstr(5, 10, 'cookies')
stdscr.refresh()
time.sleep(1)

curses.endwin()
```
