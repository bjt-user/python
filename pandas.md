#### installation on debian

```
pip3 install pandas --break-system-packages
``` 

#### convert sqlite3 table into html table

When you have a sqlite3 db in the same folder with a table named "workers" you can do this:
```
import os
import pandas
import sqlite3

connection = sqlite3.connect("test.db")

cursor = connection.cursor()

myresult = connection.execute("select * from workers").fetchall()

print(myresult)

connection.close()

df = pandas.DataFrame()
for x in myresult:
    df2 = pandas.DataFrame(list(x)).T
    df = pandas.concat([df, df2])

df.to_html('sql-data.html')
```
Now do
```
firefox sql-data.html
``` 
to view your table in html

to not display the index before each row:
```
df.to_html('templates/sql-data.html', index=False)
```

to make the border thicker:
```
df.to_html('templates/sql-data.html', index=False, border=3)
```

to align everything left:
```
df.to_html('templates/sql-data.html', index=False, border=3, justify="left")
```
or use `"center"`

https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_html.html


***

#### renaming headers of a DataFrame

```
...
df.columns = ["name", "age"]

df.to_html('sql-data.html', index=0)
```
