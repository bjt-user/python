https://packaging.python.org/en/latest/guides/installing-stand-alone-command-line-tools/

> pipx solves this by creating a virtual environment for each package, while also ensuring that its applications are accessible through a directory that is on your $PATH.\
This allows each package to be upgraded or uninstalled without causing conflicts with other packages, and allows you to safely run the applications from anywhere.

#### requirements file

At the moment there seems to be no convenient way to install python dependencies from a requirements file.