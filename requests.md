```
sudo pacman -S python-requests
```

```
import requests

r = requests.get('https://google.com')

r.text
```

To get http headers:
```
r.headers
```

Also useful:
```
r.status_code
```

```
r.ok
```
