https://docs.python.org/3/library/pdb.html

You can also invoke pdb from the command line to debug other scripts. For example:
```
python -m pdb myscript.py
```

#### pdb3

(this binary doesnt seem to exist in Arch)

```
pdb3 basic_table.py
```

`help`\
`l` - list code\
`next`\
...
