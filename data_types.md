#### print the type of a variable

You can use the `type()` function to print the type of a variable:
```
print(type(myvariable))
```

***
