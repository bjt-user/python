#### resources

https://docs.python.org/3/installing/index.html

https://pip.pypa.io/en/stable/index.html

```
man -k pip
```

#### general

> A virtual environment is a semi-isolated Python environment that allows packages to be installed for use by a particular application, rather than being installed system wide.

> On Linux systems, a Python installation will typically be included as part of the distribution. Installing into this Python installation requires root access to the system, and may interfere with the operation of the system package manager and other components of the system if a component is unexpectedly upgraded using pip.
> On such systems, it is often better to use a virtual environment or a per-user installation when installing packages with pip.

#### TODO: pip vs. pipx

#### installation

https://wiki.archlinux.org/title/Python#Package_management

```
sudo pacman -S python-pip
```

***

```
pip list
```

#### installing packages

On Debian.

It looks like you cant install packages the normal way anymore:
```
$ pip install flask
error: externally-managed-environment

× This environment is externally managed
╰─> To install Python packages system-wide, try apt install
    python3-xyz, where xyz is the package you are trying to
    install.
    
    If you wish to install a non-Debian-packaged Python package,
    create a virtual environment using python3 -m venv path/to/venv.
    Then use path/to/venv/bin/python and path/to/venv/bin/pip. Make
    sure you have python3-full installed.
    
    If you wish to install a non-Debian packaged Python application,
    it may be easiest to use pipx install xyz, which will manage a
    virtual environment for you. Make sure you have pipx installed.
    
    See /usr/share/doc/python3.11/README.venv for more information.

note: If you believe this is a mistake, please contact your Python installation or OS distribution provider. You can override this, at the risk of breaking your Python installation or OS, by passing --break-system-packages.
hint: See PEP 668 for the detailed specification.
```

On Arch a similar message appeared.

Heres how I installed `requests` on Arch:
```
sudo pacman -S python-requests
```

#### installing modules when its not available in system's package manager

```
$ pip3 install pyvmomi --break-system-packages
Defaulting to user installation because normal site-packages is not writeable
Collecting pyvmomi
  Downloading pyvmomi-8.0.2.0.1.tar.gz (852 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 852.2/852.2 kB 3.5 MB/s eta 0:00:00
  Installing build dependencies ... done
  Getting requirements to build wheel ... done
  Installing backend dependencies ... done
  Preparing metadata (pyproject.toml) ... done
Requirement already satisfied: six>=1.7.3 in /usr/lib/python3.11/site-packages (from pyvmomi) (1.16.0)
Building wheels for collected packages: pyvmomi
  Building wheel for pyvmomi (pyproject.toml) ... done
  Created wheel for pyvmomi: filename=pyvmomi-8.0.2.0.1-py2.py3-none-any.whl size=527673 sha256=bc06267cb860bff491e9cc01f440ced57955c936f4438d9aa09187a540bc7b99
  Stored in directory: /home/bf/.cache/pip/wheels/58/00/35/035f172707b539c0edd6e086053a4f177a424c0ab807799d1f
Successfully built pyvmomi
Installing collected packages: pyvmomi
Successfully installed pyvmomi-8.0.2.0.1
```
```
$ pip3 show pyvmomi | grep -i 'location'
Location: /home/bf/.local/lib/python3.11/site-packages
```

This was only installed for the current user.\
You need sudo to install it in `/usr/lib/python3.11/site-packages`

#### uninstalling packages
```
$ pip3 uninstall pyvmomi --break-system-packages
Found existing installation: pyvmomi 8.0.2.0.1
Uninstalling pyvmomi-8.0.2.0.1:
  Would remove:
    /home/bf/.local/lib/python3.11/site-packages/pyVim/*
    /home/bf/.local/lib/python3.11/site-packages/pyVmomi/*
    /home/bf/.local/lib/python3.11/site-packages/pyvmomi-8.0.2.0.1.dist-info/*
Proceed (Y/n)? y
  Successfully uninstalled pyvmomi-8.0.2.0.1
```

#### where are packages installed?

```
$ pip show requests
Name: requests
Version: 2.31.0
Summary: Python HTTP for Humans.
Home-page: https://requests.readthedocs.io
Author: Kenneth Reitz
Author-email: me@kennethreitz.org
License: Apache 2.0
Location: /usr/lib/python3.11/site-packages
Requires: charset-normalizer, idna, urllib3
Required-by:
```

#### requirements file

```
man pip-install
```

You can print what would be installed by doing a dryrun:
```
pip install --user -r requirements.txt --dry-run
```

example file:
```
ansible
ansible-core==2.16.6
ansible-lint
requests
```

You can uninstall the packages listed in the file like this:
```
pip uninstall --user -r requirements.txt
```