## xml basics

Apparently every xml file needs one root.\
A root is the unique tag that contains all other tags.\
`quizfile` is the root of this file:
```
<?xml version="1.0"?>
<quizfile>
  <quizblock>
    <...>
  </quizblock>
  <quizblock>
    <...>
  </quizblock>
</quizfile>
```

## xml.etree.ElementTree

#### print name of the root tag and the number of its child elements
```
import xml.etree.ElementTree as ET

tree = ET.parse('myquiz.xml')
root = tree.getroot()

print(root.tag)
print(len(root))
```

#### get data by indexes

given this input file:
```
<?xml version="1.0"?>
<quizfile>
  <quizblock>
    <question>Name two programming languages!</question>
    <answer>python</answer>
    <answer>c</answer>
  </quizblock>
  <quizblock>
    <question>foo?</question>
    <answer>bar</answer>
  </quizblock>
</quizfile>
```

this code
```
import xml.etree.ElementTree as ET

tree = ET.parse('myquiz_kubernetes.xml')
root = tree.getroot()

print(root[0][0].tag)
print(root[0][0].text)
```
will write this to stdout:
```
question
Name two programming languages!
```

#### loop through tags

Given the xml file above this code
```
import xml.etree.ElementTree as ET

tree = ET.parse('myquiz_kubernetes.xml')
root = tree.getroot()

for child in root:
    print(child.tag)
```

will produce this stdout:
```
quizblock
quizblock
quizblock
```

#### find

This would print the text of the first question of the first quizblock element:
```
print(root[0].find('question').text)
```
(this is only useful if you only have one question in the tag)
