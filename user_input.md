#### confirmation

```
if (input("Continue? [Y/y]").lower().startswith('y')):
    // continue with the program
else:
    exit(1)
```

#### get list from user input

```
mylist = []

while True:
    new_item = input("Insert new item (enter to proceed): ")
    if new_item == "":
        break
    else:
        mylist.append(new_item)

print(mylist)
```
