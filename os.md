#### get the home of a user

Get the home of the user executing the python script:
```
os.path.expanduser('~')
```

Get the home of a specific user:
```
os.path.expanduser('~ansible')
```
