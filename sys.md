Sometimes (for example for `pyinstaller`) you need to explicitly import sys\
to use `exit()`.

#### sys.exit()

```
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).
```

```
sys.exit(2)
```

or

```
sys.exit("Something went wrong!")
```
