https://docs.python.org/3/tutorial/datastructures.html#dictionaries

#### example

```
>>> my_dict = {'name': 'Bodo', 'gender': 'male'}
>>> type(my_dict)
<class 'dict'>
```

```
>>> my_dict['gender']
'male'
>>> my_dict['name']
'Bodo'
```

#### nested dict

```
>>> nested_dict = {
... "person1": {
... "name": "Mike",
... "age": 42
... },
... "person2": {
... "name": "Tom",
... "age": 50
... }
... }
```

```
>>> nested_dict["person2"]["age"]
50
```

#### example in a script with variables

```
computer_name = input("What is the name of the computer? ")

computer_brand = input("What is the brand of the computer? ")

computer_price = input("How much does the computer cost? ")

computer_data = {
    "computer_name": computer_name,
    "computer_brand": computer_brand,
    "computer_price": computer_price
}
```

#### check if key is present in dictionairy

```
if "foo" in mydict:
    print("true")
```
This will print true if `"foo"` is a key in the dict `mydict`.

#### show top level keys of dict

```
>>> mydict
{'foo': 'bar', 'customers': {'accounts': 'ok', 'root': 'no'}}
>>> mydict.keys()
dict_keys(['foo', 'customers'])
```

To get a list:
```
>>> list(mydict)
['foo', 'customers']
```

> Performing list(d) on a dictionary returns a list of all the keys used in the dictionary, in insertion order (if you want it sorted, just use sorted(d) instead).\
To check whether a single key is in the dictionary, use the in keyword.

#### show all values of dict

```
>>> mydict.values()
dict_values(['bar', {'accounts': 'ok', 'root': 'no'}])
```

to get a list:
```
>>> list(mydict.values())
['bar', {'accounts': 'ok', 'root': 'no'}]
```

#### comparing dicts

```
>>> mydict = {
... "foo": "bar",
... "test": "ok"
... }
>>> seconddict = {
... "foo": "bar",
... "test": "ok"
... }
>>> mydict == seconddict
True
>>> seconddict["foo"] = "BAR"
>>> mydict == seconddict
False
>>> seconddict
{'foo': 'BAR', 'test': 'ok'}
>>>
```

The order of keys does not seem to matter:
```
>>> mydict = {
... "foo": "bar",
... "test": "ok"
... }
>>> seconddict = {
... "test": "ok",
... "foo": "bar"
... }
>>> mydict == seconddict
True
```

TODO: Check if the keys of two dicts are the same
