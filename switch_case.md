They are called `match statements`:

https://docs.python.org/3/tutorial/controlflow.html#match-statements

#### example

```
myinput = int(input("type in a number: "))

match myinput:
    case 0:
        print("ZERO")
    case 1:
        print("ONE")
    case 2:
        print("TWO")
```
