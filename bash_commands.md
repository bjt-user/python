#### execute bash commands

```
>>> import subprocess
>>> subprocess.run('df -h', shell = True, executable="/bin/bash")
```

#### save output of bash command in variable

```
>>> my_var = subprocess.check_output('df -h', shell = True, executable="/bin/bash", stderr = subprocess.STDOUT)
>>> print (my_var)
b'Filesystem      Size  Used Avail Use% Mounted on\nnone            3.8G  4.0K  3.8G   1% /mnt/wsl\nnone            477G  164G  314G  35% /usr/lib/wsl/drivers\n/dev/sdc       1007G   45G  912G   5% /\nnone            3.8G  136K  3.8G   1% /mnt/wslg\nnone            3.8G     0  3.8G   0% /usr/lib/wsl/lib\nrootfs          3.8G  2.0M  3.8G   1% /init\nnone            3.8G     0  3.8G   0% /dev\nnone            3.8G  484K  3.8G   1% /run\nnone            3.8G     0  3.8G   0% /run/lock\nnone            3.8G     0  3.8G   0% /run/shm\ntmpfs           4.0M     0  4.0M   0% /sys/fs/cgroup\nnone            3.8G   76K  3.8G   1% /mnt/wslg/versions.txt\nnone            3.8G   76K  3.8G   1% /mnt/wslg/doc\ndrvfs
 477G  164G  314G  35% /mnt/c\ntmpfs           3.8G     0  3.8G   0% /tmp\n'
 ```
