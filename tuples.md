A `tuple` looks like this:
```
>>> mytuple=("one", "two", "three")
>>> type(mytuple)
<class 'tuple'>
```

#### cast tuple to list

```
mytuple = list(mytuple)
```
