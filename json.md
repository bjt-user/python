#### load json file into dictionairy

```
>>> my_json_file = open('config.json', 'r')
>>> json_data = json.load(my_json_file)
>>> json_data
{'name': 'foo'}
```

#### example: parse and pretty print json

Open a json file save the content to a string.\
Then parse it, extract a value of a key and pretty print it.
```
#!/usr/bin/python3

import json

my_file = open("example.json", "r")
file_content = my_file.read()
my_file.close()

json_dict = json.loads(file_content)

print(json_dict['glossary']['title'])

print('\nnow pretty print:')
print(json.dumps(json_dict, indent=4))

exit(0)
```

This worked for this example.json file:
```
{"glossary":{"title":"example glossary","GlossDiv":{"title":"S","GlossList":{"GlossEntry":{"ID":"SGML","SortAs":"SGML","GlossTerm":"Standard Generalized Markup Language","Acronym":"SGML","Abbrev":"ISO 8879:1986","GlossDef":{"para":"A meta-markup language, used to create markup languages such as DocBook.","GlossSeeAlso":["GML","XML"]},"GlossSee":"markup"}}}}}
```

#### write json to a file

You should use a `dict` variable.\
And you need to open your file so you have a file handle.

```
my_name = 'Joe'
json_data = { "name": my_name }

config_file_handle = open(CONFIG_FILE, 'w')
json.dump(json_data, config_file_handle)
```

This will write valid json to your config file like this:
```
{"name": "Joe"}
```

**But** the above statement will put the entire json in one line into the file.\
To have a file that is already pretty printed, you need the `indent=4` as last argument:
```
json.dump(json_data, config_file_handle, indent=4)
```

#### character encoding

https://docs.python.org/3/library/json.html#character-encodings

It is writing ASCII to files by default.\
Use `ensure_ascii=False`:
```
json.dump(config_dict, config_file_handle, indent=4, ensure_ascii=False)
```
(this is when writing json to a file)
