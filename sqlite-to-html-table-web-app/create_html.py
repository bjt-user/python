import os
import pandas
import sqlite3

connection = sqlite3.connect("test.db")

cursor = connection.cursor()

myresult = connection.execute("select * from workers").fetchall()

print(myresult)

connection.close()

df = pandas.DataFrame()
for x in myresult:
    df2 = pandas.DataFrame(list(x)).T
    df = pandas.concat([df, df2])

df.to_html('templates/sql-data.html')
