#### ANDing conditionals

```
if (varone == 3) & (vartwo == 4):
    print("BING")
```

the `and` keyword also works:
```
if (varone == 3) and (vartwo == 4):
    print("BINGO")
```

#### breaking lines to avoid long if lines

This is one way to do it:
```
if (
    myvar == 3
    or
    mysecondvar == 4
    ):
    print("BINGO")
else:
    print("HELL NO!")
```

Or you can use this syntax:
```
if os.path.exists(path_one) \
    or os.path.exists(path_two) \
    or os.path.exists(path_three):
    print("foo")
```
