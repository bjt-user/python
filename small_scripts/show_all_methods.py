#!/usr/bin/python3

print('For which module do you want to see all available methods?')
chosen_module = input()

chosen_module = __import__(chosen_module)

for element in dir(chosen_module):
    if element == element.lower():
        print(element)

exit(0)
