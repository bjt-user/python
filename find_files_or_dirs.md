#### glob module

```
>>> os.chdir('/etc')
>>> os.getcwd()
'/etc'
>>> import glob
>>> glob.glob('*.conf')
['dnsmasq.conf', 'mtools.conf', 'request-key.conf', 'rsyncd.conf', 'host.conf', 'mke2fs.conf', 'sudo.conf', 'i3blocks.conf', 'sensors3.conf', 'proxychains.conf', 'resolv.conf', 'mkinitcpio.conf', 'signond.conf', 'nsswitch.conf', 'libva.conf', 'man_db.conf', 'mdadm.conf', 'krb5.conf', 'libaudit.conf', 'xattr.conf', 'i3status.conf', 'ts.conf', 'sudo_logsrvd.conf', 'logrotate.conf', 'healthd.conf', 'vconsole.conf', 'fuse.conf', 'nftables.conf', 'e2scrub.conf', 'gai.conf', 'ld.so.conf', 'pacman.conf', 'named.conf', 'locale.conf', 'makepkg.conf']
```

#### os.walk()

https://docs.python.org/3/library/os.html#os.walk

```
>>> import os
>>> help(os.walk)
```

```
>>> import os
>>> walk_object = os.walk('.')
>>> walk_object
<generator object walk at 0x7bf6c793ca50>
>>> type(walk_object)
<class 'generator'>
```

```
Generator functions
-------------------

A function or method which uses the "yield" statement (see section The
yield statement) is called a *generator function*.  Such a function,
when called, always returns an *iterator* object which can be used to
execute the body of the function:  calling the iterator’s
"iterator.__next__()" method will cause the function to execute until
it provides a value using the "yield" statement.  When the function
executes a "return" statement or falls off the end, a "StopIteration"
exception is raised and the iterator will have reached the end of the
set of values to be returned.
```

```
>>> walk_object.__next__()
('.', ['foo', 'bar'], ['fileone', 'filetwo', 'alink'])
>>> walk_object.__next__()
('./foo', [], [])
>>> walk_object.__next__()
('./bar', [], [])
>>> walk_object.__next__()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
```

Weird iterations.\
When you have exactly three elements in your for loop something weird happens:
```
>>> for element in os.walk('.'):
...     print(element)
...
('.', ['foo', 'bar'], ['fileone', 'filetwo', 'alink'])
('./foo', [], [])
('./bar', [], [])
>>> for one, two, three in os.walk('.'):
...     print(one)
...     print(two)
...     print(three)
...
.
['foo', 'bar']
['fileone', 'filetwo', 'alink']
./foo
[]
[]
./bar
[]
[]
>>> for one, two in os.walk('.'):
...     print(one)
...     print(two)
...
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: too many values to unpack (expected 2)
```

Don't print the root directory itself:
```
for element in os.walk('.'):
    if (element[0] != '.'):
        print(element)
```

search for directories that contain a substring:
```
>>> for root, dirs, files in os.walk('.'):
...     if ("fo" in root):
...             print(root)
...
./foo
```
