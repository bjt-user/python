Print without newline:
```
print("foo", end='')
```

#### print variables and text

You can do it with commas as separators:
```
print('The HISTORY_FILE', HISTORY_FILE, 'was created.')
```
(This is nice because spaces are automatically inserted.)

Or concatenation:
```
print('the var ' + myvar + ' is being printed.')
```
