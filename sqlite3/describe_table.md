For some reason `.schema mytable` doesnt work, so you have to use this:
```
import sqlite3

connection = sqlite3.connect("test.db")

cursor = connection.cursor()

a = connection.execute("PRAGMA table_info('workers')")

print(a.fetchall())

connection.close()
```

#### get table headers

```
table_info = connection.execute("PRAGMA table_info(\"workers\")").fetchall()

headers = []
for row in table_info:
    row = list(row)
    headers.append(row[1])
```
