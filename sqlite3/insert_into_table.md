**You have to do a commit after inserting!**

```
import sqlite3

connection = sqlite3.connect("mydb.db")

cursor = connection.cursor()

print ("What to insert in column1?")
column1 = input()

print ("What to insert in column2?")
column2 = input()

sql = "INSERT INTO orders (column1, column2)"
sql = sql + " VALUES (\"" + column1 + "\", \" + column2 + "\")"

cursor.execute(sql)

connection.commit()

connection.close()
```
