It's important to use the `.fetchall()` function to make the sqlite3 object into a list.

#### select only one element from the list

```
import sqlite3

connection = sqlite3.connect("test.db")

cursor = connection.cursor()

names = connection.execute("select name from workers").fetchall()

print(names[0])

connection.close()
```

#### select the first row

```
import sqlite3

connection = sqlite3.connect("test.db")

cursor = connection.cursor()

names = connection.execute("select * from workers").fetchall()

print(names[0])

connection.close()
```

#### select the second column of the first row

```
import sqlite3

connection = sqlite3.connect("test.db")

cursor = connection.cursor()

names = connection.execute("select * from workers").fetchall()

print(names[0][1])

connection.close()
``` 

#### sort the list

```
import sqlite3

connection = sqlite3.connect("test.db")

cursor = connection.cursor()

names = connection.execute("select name from workers").fetchall()

print(sorted(names))

connection.close()
```
***
