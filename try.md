#### catch general errors

```
try:
    my_file = open(my_file_path, "r")
except Exception as error:
    print("Failed to open file", my_file_path)
    print(error)
```

#### catch specific error

Print custom error message and the error message that comes from the library:
```
try:
    json_data = json.load(config_file_handle)
except json.decoder.JSONDecodeError as error:
    print("Failed to load json file", config_file)
    print("Invalid JSON!")
    print(error)
```
