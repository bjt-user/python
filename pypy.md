You can try to use the package `pypy3` to make python faster.

#### usage

```
$ python3 hw.py 
hi
$ pypy3 hw.py 
hi
```
